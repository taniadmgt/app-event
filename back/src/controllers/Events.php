<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\EventsModel;

class Events extends Controller {
  protected object $event;

  public function __construct($param) {
    $this->event = new EventsModel();

    parent::__construct($param);
  }

  public function postEvents() {
    $this->event->add($this->body);

    return $this->event->getLast();
  }

  public function deleteEvents() {
    return $this->event->delete(intval($this->params['id']));
  }

  public function getEvents() {
    return $this->event->get(intval($this->params['id']));
  }
}
