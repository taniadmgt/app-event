<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class Login extends Controller {
  protected object $user;

  public function __construct($param) {
    $this->user = new UserModel();

    parent::__construct($param);
  }

  public function postLogin() {
    $this->user->add($this->body);

    return $this->user->getLast();
  }

  public function deleteLogin() {
    return $this->user->delete(intval($this->params['id']));
  }

  public function getLogin() {
    return $this->user->get(intval($this->params['email'], $this->params['password']));
  }
}

