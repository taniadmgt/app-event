<?php

namespace App\Controllers;

use App\Controllers\Controller;
use App\Models\UserModel;

class Register extends Controller {
  protected object $user;

  public function __construct($param) {
    $this->user = new UserModel();

    parent::__construct($param);
  }

  public function postRegister() {
    return $this->checkEmail();
    die;
    $this->user->add($this->body);

    return $this->user->getLast();
  }

  public function checkEmail() {
    return $this->body['email']; 
  }

  public function checkPassword() {
    return $this->body['password']; 
  }

  public function deleteRegister() {
    return $this->user->delete(intval($this->params['id']));
  }

  public function getRegister() {
    return $this->user->get(intval($this->params['id']));
  }
}

