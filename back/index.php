<?php

require 'vendor/autoload.php';

use App\Router;
use App\Controllers\User;
use App\Controllers\Events;
use App\Controllers\Register;
use App\Controllers\Login;

new Router([
  'users/:id' => User::class,
  'events' => Events::class,
  'events/:id' => Events::class,
  'users' => User::class,
  'users/create' => Register::class,
  'login/:email/:password' => Login::class,
]);
